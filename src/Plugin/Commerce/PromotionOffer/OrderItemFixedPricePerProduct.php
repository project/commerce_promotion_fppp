<?php

namespace Drupal\commerce_promotion_fppp\Plugin\Commerce\PromotionOffer;

use Drupal\commerce_order\Adjustment;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_promotion\Entity\PromotionInterface;
use Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\OrderItemPromotionOfferBase;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a fixed price per each matching product.
 *
 * @CommercePromotionOffer (
 *   id = "commerce_promotion_fppp_fixed_price_per_product",
 *   label = @Translation("Fixed price per product"),
 *   entity_type = "commerce_order_item",
 * )
 */
class OrderItemFixedPricePerProduct extends OrderItemPromotionOfferBase {

  /**
   * {@inheritdoc}
   */
  public function apply(EntityInterface $entity, PromotionInterface $promotion) {
    $this->assertEntity($entity);
    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
    $order_item = $entity;
    $adjusted_total_price = $order_item->getAdjustedTotalPrice(['promotion']);
    $purchased_entity = $order_item->getPurchasedEntity();
    if ($purchased_entity instanceof ProductVariation) {
      $product_id = $purchased_entity->getProduct()->id();
    }
    else {
      return;
    }
    // Get the promotion price.
    $index = array_search($product_id, array_column($this->configuration['fppp_prices_products'], 'product'));
    if (is_int($index)) {
      $promotion_price = $this->configuration['fppp_prices_products'][$index]['price'];
      if (empty($promotion_price)) {
        return;
      }
    }
    else {
      return;
    }
    $promotion_price = new Price($promotion_price['number'], $promotion_price['currency_code']);

    // Return if different currency.
    if ($adjusted_total_price->getCurrencyCode() != $promotion_price->getCurrencyCode()) {
      return;
    }

    $unit_price = $order_item->getUnitPrice();
    $actual_discount = $unit_price->subtract($promotion_price);
    // Actual discount should be positive.
    if (!$actual_discount->isPositive()) {
      return;
    }

    // Handle display inclusive.
    // Use the same logic as order_item_percentage_off.
    if ($this->configuration['display_inclusive']) {
      // First, get the adjusted unit price to ensure the order item is not
      // already fully discounted.
      $adjusted_unit_price = $order_item->getAdjustedUnitPrice(['promotion']);
      // The adjusted unit price is already reduced to 0, no need to continue
      // further.
      if ($adjusted_unit_price->isZero()) {
        return;
      }
      // Display-inclusive promotions must first be applied to the unit price.
      if ($actual_discount->greaterThan($adjusted_unit_price)) {
        // Don't reduce the unit price past zero.
        $actual_discount = $adjusted_unit_price;
      }
      $unit_price = $order_item->getUnitPrice();
      $new_unit_price = $unit_price->subtract($actual_discount);
      $order_item->setUnitPrice($new_unit_price);
      $adjustment_amount = $actual_discount->multiply($order_item->getQuantity());
      $adjustment_amount = $this->rounder->round($adjustment_amount);
    }
    else {
      $adjustment_amount = $actual_discount->multiply($order_item->getQuantity());
      $adjustment_amount = $this->rounder->round($adjustment_amount);
    }

    // Skip applying the promotion if there's no amount to discount.
    if ($adjustment_amount->isZero()) {
      return;
    }

    $order_item->addAdjustment(new Adjustment([
      'type' => 'promotion',
      'label' => $promotion->getDisplayName() ?: $this->t('Discount'),
      'amount' => $adjustment_amount->multiply('-1'),
      'source_id' => $promotion->id(),
      'included' => $this->configuration['display_inclusive'],
    ]));
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'fppp_prices_products' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $wrapper_id = Html::getUniqueId('commerce-promotion-fppp-prices-products');
    $form['fppp_prices_products'] = [
      '#tree' => TRUE,
      '#weight' => -1,
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
    ];
    $prices_products = $this->configuration['fppp_prices_products'];
    $prices_products_number = $form_state->get('fppp_prices_products_number');
    if (!$prices_products_number) {
      $user_input = (array) NestedArray::getValue($form_state->getUserInput(), $form['#parents']);
      if (isset($user_input['fppp_prices_products'])) {
        $prices_products = $user_input['fppp_prices_products'];
      }
      $prices_products_number = count(array_filter(array_keys($prices_products), 'is_int'));
      $prices_products_number = $prices_products_number ?: 1;
      $form_state->set('fppp_prices_products_number', $prices_products_number);
    }
    for ($i = 0; $i < $prices_products_number; $i++) {
      $form['fppp_prices_products'][$i] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['container container-inline'],
        ],
      ];
      $form['fppp_prices_products'][$i]['price'] = [
        '#type' => 'commerce_price',
        '#title' => $this->t('Price'),
        '#default_value' => !empty($prices_products[$i]['price']) ? $prices_products[$i]['price'] : NULL,
        '#weight' => -1,
      ];
      $form['fppp_prices_products'][$i]['product'] = [
        '#type' => 'entity_autocomplete',
        '#target_type' => 'commerce_product',
        '#default_value' => !empty($prices_products[$i]['product']) ? Product::load($prices_products[$i]['product']) : NULL,
        '#title' => $this->t('Product'),
        '#selection_handler' => 'default',
      ];
    }
    $form['fppp_prices_products']['add_one'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one'),
      '#submit' => [__CLASS__ . '::addOneCallback'],
      '#ajax' => [
        'callback' => __CLASS__ . '::ajaxCallback',
        'wrapper' => $wrapper_id,
      ],
    ];

    $form['fppp_prices_products']['remove_one'] = [
      '#type' => 'submit',
      '#value' => $this->t('Remove last one'),
      '#submit' => [__CLASS__ . '::removeLastRowCallback'],
      '#ajax' => [
        'callback' => __CLASS__ . '::ajaxCallback',
        'wrapper' => $wrapper_id,
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      foreach (array_filter(array_keys($values['fppp_prices_products']), 'is_int') as $key) {
        if (empty($values['fppp_prices_products'][$key]['price']) || empty($values['fppp_prices_products'][$key]['product'])) {
          unset($values['fppp_prices_products'][$key]);
        }
      }
      $this->configuration['fppp_prices_products'] = $values['fppp_prices_products'];
      $this->setConditionProducts($values['fppp_prices_products']);
    }
  }

  /**
   * @param array $prices_products
   *
   * @return void
   */
  protected function setConditionProducts(array $prices_products) {
    $condition_products = [];
    foreach (array_filter(array_keys($prices_products), 'is_int') as $key) {
      if (!empty($prices_products[$key]['product'])) {
        $condition_products[] = ['product' => Product::load($prices_products[$key]['product'])->uuid()];
      }
    }
    if (!empty($condition_products)) {
      // Allow only products as conditions.
      $this->configuration['conditions'] = [
        [
          'plugin'  => 'order_item_product',
          'configuration' => ['products' => $condition_products],
        ],
      ];
    }
  }

  /**
   * Callback to add one row at the end.
   *
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public static function addOneCallback($form, FormStateInterface $form_state) {
    $number = $form_state->get('fppp_prices_products_number');
    $form_state->set('fppp_prices_products_number', $number + 1);
    $form_state->setRebuild();
  }

  /**
   * Callback to remove the last row.
   *
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public static function removeLastRowCallback($form, FormStateInterface $form_state) {
    $number = $form_state->get('fppp_prices_products_number');
    if ($number && $number > 1) {
      $form_state->set('fppp_prices_products_number', $number - 1);
    }
    $form_state->setRebuild();
  }

  /**
   * The ajax callback of the form.
   *
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public static function ajaxCallback($form, FormStateInterface $form_state) {
    $parents = $form_state->getTriggeringElement()['#array_parents'];
    $parents = array_slice($parents, 0, -1);
    return NestedArray::getValue($form, $parents);
  }

}
