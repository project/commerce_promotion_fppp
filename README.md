# Commerce Promotion Fixed Price per Product

## Usage

Inside promotion add/edit form there is a new offer type called "Fixed price per product", when it is selected, it appears one row with "Price" and "Product" fields.

The price will be the actual discounted price, e.g if the current price of the product is 10 and you put 8
the price will be 8 and the discount 2.

By clicking "Add one", you can add another price/product etc.

## Notes

* Leave the checkboxes "Applies to Specific products" unchecked as it will automatically fill the products from the values above.
* For the moment, it cannot work with other conditions except products.


## Todo

* Check Drupal 10 compatibility.
* Check different currencies.
* Tests.
